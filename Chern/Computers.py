#!/usr/bin/env python
##########################################################################################
# This python module calculates the quantities stored in the Assemblers.py module for
# different values of the tuning parameters.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import os
import numpy as np
import scipy.integrate as integrate

# custom modules:
from .Assemblers import *
###########################################################################################


#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.2"


####################################
########     COMPUTERS     #########
####################################
###########################################################################################
def compute_curv_func_bare(M, kx_left, kx_right, Nx, ky_left, ky_right, Ny):
    """
        Computes the (noninteracting) curvature function over the whole Brillouin zone.
       
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
                   
        kx_left: float, lower bound for the kx values.

        kx_right: float, upper bound for the kx values.
        
        Nx: integer, number of points in the momentum sum in x direction.
           
        ky_left: float, lower bound for the ky values.

        ky_right: float, upper bound for the ky values.

        Ny: integer, number of points in the momentum sum in y direction.

        Returns
        -------
        curv_func_array: numpy array, curvature function at all kx, ky for given parameters
           
        References
        ----------

        Notes
        -----

        Examples
        --------

    """

    #initialization:
    curv_func_array = np.zeros(shape=(Ny,Nx))
    kx_array = np.linspace(kx_left,kx_right,Nx)
    ky_array = np.linspace(ky_left,ky_right,Ny)

    # Output the current status of the program:
    print("\n")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("Working on point M="+str(M))
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("\n")
    
    # Loop over the whole Brillouin zone and calculate the curvature function via subroutine:
    for kx_idx,kx in enumerate(kx_array):
        for ky_idx,ky in enumerate(ky_array):
            curv_func_array[ky_idx][kx_idx] = curv_func_bare(M, kx, ky)
        print((kx_idx*len(ky_array) + ky_idx)/(len(kx_array)*len(ky_array))*100, "% of calculation of curvature function completed.")

    # Saving data:
    data = {'curv_func': curv_func_array}
    np.save("data/Chern/curv-func-bare-M-{0}-kx-{1}-{2}-{3}-ky-{4}-{5}-{6}.npy".format(M,kx_left,kx_right,Nx,ky_left,ky_right,Ny), data)

    return curv_func_array
######################################################################################

######################################################################################
def compute_curv_func(M, u, vs, kx_left, kx_right, Nx, ky_left, ky_right, Ny, omega):
    """
        Computes the (interacting) curvature function over the whole Brillouin zone.
       
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
                   
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.
                
        kx_left: float, lower bound for the kx values.

        kx_right: float, upper bound for the kx values.
        
        Nx: integer, number of points in the momentum sum in x direction.
           
        ky_left: float, lower bound for the ky values.

        ky_right: float, upper bound for the ky values.

        Ny: integer, number of points in the momentum sum in y direction.
        
        omega: float, Matsubara frequency.

        Returns
        -------
        curv_func_array: numpy array, curvature function at all kx, ky for given parameters
           
        References
        ----------

        Notes
        -----

        Examples
        --------

    """

    # Initialization:
    curv_func_array = np.zeros(shape=(Ny,Nx), dtype=np.complex_)
    kx_array = np.linspace(kx_left,kx_right,Nx)
    ky_array = np.linspace(ky_left,ky_right,Ny)

    # Loop over all points in the Brillouin zone and calculate the full interacting
    # curvature function from subroutine:
    for kx_idx,kx in enumerate(kx_array):
        for ky_idx,ky in enumerate(ky_array):
            curv_func_array[ky_idx][kx_idx] = curv_func(M, u, vs, kx, ky, omega)
            print((kx_idx*len(ky_array) + ky_idx)/(len(kx_array)*len(ky_array))*100, "% of calculation of curvature function completed.")

    # Saving data:
    data = {'curv_func': curv_func_array}
    np.save("data/Chern/curv-func-M-{0}-u-{1}-vs-{2}-kx-{3}-{4}-{5}-ky-{6}-{7}-{8}-omega-{9}.npy".format(M,u,vs,kx_left,kx_right,Nx,ky_left,ky_right,Ny,omega), data)

    return curv_func_array
######################################################################################

######################################################################################
def compute_top_inv_bare(M, Nx, Ny, save):
    """
        Computes the topological invariant for the noninteracting system at the given parameters.
       
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
                   
        Nx: integer, number of points in the momentum sum in x direction.

        Ny: integer, number of points in the momentum sum in y direction.
        
        save: boolean, flag to save the data.

        Returns
        -------
        top_inv: float, topological invariant
           
        References
        ----------

        Notes
        -----

        Examples
        --------

    """

    # Initialization:
    curv_func_array = compute_curv_func_bare(M, -np.pi, np.pi, Nx, -np.pi, np.pi, Ny)
    kx_step = 2*np.pi/Nx
    ky_step = 2*np.pi/Ny
    top_inv = np.sum(np.sum(curv_func_array))*kx_step*ky_step

    if save==True:
        # Saving data:
        data = {'top_inv': top_inv}
        np.save("data/Chern/curv-func-bare-M-{0}-Nx-{1}-Ny-{2}.npy".format(M,Nx,Ny), data)

    return top_inv
######################################################################################


######################################################################################
def get_training_set_all_HSPs(M_initial, M_final, M_points, random_flag):
    """
        Collects the values of the curvature function at the high-symmetry points (0,0), (pi,0), (pi,pi) to use to train the NN.
       
        Parameters
        ----------
        M_initial: float, initial value in the range for the mass term in the Hamiltonian.

        M_final: float, final value in the range for the mass term in the Hamiltonian.

        M_points: integer, total number of points in the range for the mass term in the Hamiltonian.
        
        random_flag: boolean, flag to randomly sample points in the M interval.

        Returns
        -------
        curv_func_array: numpy array, curvature function at the HSPs for given parameters
           
        References
        ----------

        Notes
        -----
        The training set contains only points in the non-interacting parameter space.

        Examples
        --------

    """
    
    # Initialization
    curv_func_array=np.zeros(shape=(M_points,3))
    top_inv_array=np.zeros(M_points)
    # The array of M points can be filled with randomly selected points or equally-spaced points in the given interval:
    if random_flag==True:
        M_array=np.random.uniform(M_initial,M_final,M_points)
    else:
        M_array=np.linspace(M_initial,M_final,M_points)
    
    
    # Definition of a list (of lists) of high-symmetry points
    HSPs = [[0,0],[np.pi,0],[np.pi,np.pi]]
    
    # Loop over all points to get the values of the curvature function:
    for M_idx, M in enumerate(M_array):
        for HSP_idx,HSP in enumerate(HSPs):
            kx=HSP[0]
            ky=HSP[1]
            d1 = np.sin(kx);
            d2 = np.sin(ky);
            d3 = M + 2 - np.cos(kx) - np.cos(ky)
            dmag = np.sqrt(d1**2 + d2**2 + d3**2)
            dxd1 = np.cos(kx)
            dyd2 = np.cos(ky)
            dxd3 = np.sin(kx)
            dyd3 = np.sin(ky)
            curv_func_array[M_idx,HSP_idx] = 1/(4*np.pi)*(1/dmag**3)*(-d1*dxd3*dyd2 - d2*dxd1*dyd3 + d3*dxd1*dyd2)
            
            # Print statement to monitor the progress of the data generation:
            if M_idx%64==0:
                print((M_idx*len(HSPs) + HSP_idx)/(len(M_array)*len(HSPs))*100, "% of calculation of curvature function completed.")
        
        #get labels (top invariant) analytically:
        if M<-4 or M>0:
            top_inv_array[M_idx]=0
        elif -4 < M < -2:
            top_inv_array[M_idx]=1
        elif -2 < M < 0:
            top_inv_array[M_idx]=-1
        else:
            top_inv_array[M_idx]=0

    print("Data successfully generated.")

    # Saving data (assumes folders exist):
    data = {'curv_func': curv_func_array, 'top_inv': top_inv_array}
    if random_flag==True:
        np.save("data/Chern/curv-func-HSPs-top-inv-M-{0}-{1}-{2}-rand.npy".format(M_initial,M_final,M_points), data)
    if random_flag==False:
        np.save("data/Chern/curv-func-HSPs-top-inv-M-{0}-{1}-{2}.npy".format(M_initial,M_final,M_points), data)

    return curv_func_array, top_inv_array
######################################################################################


######################################################################################
def get_test_set_all_HSPs(M_initial, M_final, M_points, vs, u_initial, u_final, u_points):
    """
        Collects the values of the curvature function at the high-symmetry points (0,0), (pi,0), (pi,pi) to use to test the NN.
       
        Parameters
        ----------
        M_initial: float, initial value in the range for the mass term in the Hamiltonian.

        M_final: float, final value in the range for the mass term in the Hamiltonian.

        M_points: integer, total number of points in the range for the mass term in the Hamiltonian.
        
        vs: float, sound velocity.

        u_initial: float, initial value in the range for phonon coupling.

        u_final: float, final value in the range for phonon coupling.

        u_points: integer, total number of points in the range for phonon coupling.
        
        Returns
        -------
        curv_func_array: numpy array, curvature function at the HSPs for given parameters
           
        References
        ----------

        Notes
        -----
        M_points should always be even to avoid sampling M=0 (training set).


        Examples
        --------

    """
    
    # Check whether the test set does not overlap with the training set:
    if M_points%2==1:
        print("You have chosen an odd number of points in the M-interval, but this samples the M=0 line, which corresponds to the non-interacting case. As a consequence, the test and training sets are not disjoint. Please choose an even number of points.")
        sys.exit()
    
    # Initialization
    M_array=np.linspace(M_initial,M_final,M_points)
    u_array=np.linspace(u_initial,u_final,u_points)
    curv_func_array=np.zeros(shape=(u_points,M_points,3)) #all parameters are equivalent "test points"
    # Definition of a list (of lists) of high-symmetry points (omega, k_x, k_y):
    # In frequency space, the only HSP is omega=0
    HSPs = [[0,0,0],[0,np.pi,0],[0,np.pi,np.pi]]
    
    # Loop over all points to get the values of the curvature function:
    for M_idx, M in enumerate(M_array):
        for u_idx, u in enumerate(u_array):
            for HSP_idx,HSP in enumerate(HSPs):
                omega=HSP[0]
                kx=HSP[1]
                ky=HSP[2]
                # Note that the curvature function is real anyway
                curv_func_array[u_idx][M_idx][HSP_idx] = np.real(curv_func(M, u, vs, kx, ky, omega))
                print('The curvature function is:',curv_func_array[u_idx][M_idx][HSP_idx])
                
            # Print statement to monitor the progress of the data generation:
            if M_idx%8 == 0:
                print((M_idx*len(u_array) + u_idx)/(len(M_array)*len(u_array))*100, "% of calculation of curvature function completed.")

    # Saving data:
    data = {'curv_func': curv_func_array}
    np.save("data/Chern/curv-func-HSPs-M-{0}-{1}-{2}-vs-{3}-u-{4}-{5}-{6}.npy".format(M_initial,M_final,M_points,vs,u_initial,u_final,u_points), data)

    return curv_func_array
######################################################################################
