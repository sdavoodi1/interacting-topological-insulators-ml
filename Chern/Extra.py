#!/usr/bin/env python
##########################################################################################
# This python module contains additional functions that were not used for the generation of
# the final results of the SciPost paper.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import os
import numpy as np
import scipy.integrate as integrate

# custom modules:
from .Assemblers import *
from .Computers import *
from .MachineLearning import *

#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.2"
###########################################################################################



###########################################################################################
def pca(X=np.array([]), no_dims=2):
    """
        Runs Principal Component Analysis on a NxD array X to reduce dimension.
        
        Parameters
        ----------
        X: numpy array, the feature vector.
        
        no_dims: integer, number of dimensions to be kept plotted in the PCA.


        Returns
        -------
        Y, numpy array, the dimensional-reduced vector after the PCA.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    (n, d) = X.shape
    X = X - np.tile(np.mean(X, 0), (n, 1))
    (l, M) = np.linalg.eig(np.dot(X.T, X))
    Y = np.dot(X, M[:, 0:no_dims])
    return Y
###########################################################################################


######################################################################################
def plot_PCA_training(M_initial, M_final, M_points, no_dims=2, load=False):
    """
        Plot a principal component analysis of the training set.
        
        Parameters
        ----------
        M_initial: float, smallest mass term in the data set.

        M_final: float, largest mass term in the data set.

        M_points: integer, number of points in the M-direction.
               
        no_dims: integer, number of dimensions to be kept plotted in the PCA.
        
        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots a visualization of the PCA.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """
    
    #initialization of the M array
    M_array=np.linspace(M_initial,M_final,M_points)

    #fetching data:
    if load==True:
        data = np.load("data/Chern/curv-func-HSPs-top-inv-M-{0}-{1}-{2}.npy".format(M_initial,M_final,M_points), allow_pickle=True, encoding='latin1')
        train_configs  = data[()]['curv_func']
        train_labels  = data[()]['top_inv']
        
    elif load==False:
        train_configs, train_labels = get_training_set_all_HSPs(M_initial, M_final, M_points)
    
    #compute PCA via subroutine:
    PCA_coord = pca(train_configs.T, no_dims)
    fig = plt.figure(dpi=100)

    #plot results:
    plt.scatter(PCA_coord[:, 0], PCA_coord[:, 1], 50, train_labels,
                marker='o', edgecolors='k', linewidths=0.5)

    plt.axis('off')
    plt.show()
    plt.close()
    
    return
######################################################################################
