#!/usr/bin/env python
##########################################################################################
# This python module visualizes the quantities calculated in the other modules.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import numpy as np

#plotting
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors
import colormaps as cmaps

from .Assemblers import *
from .Computers import *
from .MachineLearning import *
###########################################################################################





#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.2"
###########################################################################################


###########################################################################################
def plot_curv_func(M, u, vs, kx_left, kx_right, Nx, ky_left, ky_right, Ny, omega, vmin="None", vmax="None", load=False):
    """
        Plot curvature function in k-space at a given set of parameters.
        
        Parameters
        ----------
        M: float, mass term.

        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.
        
        kx_left: float, lower bound for the kx values.

        kx_right: float, upper bound for the kx values.
        
        Nx: integer, number of points in kx direction.
           
        ky_left: float, lower bound for the ky values.

        ky_right: float, upper bound for the ky values.

        Ny: integer, number of points in ky direction.
        
        omega: float, Matsubara frequency.
        
        vmin: integer, minimal value to show in the colorbar.

        vmax: integer, maximal value to show in the colorbar.

        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots the curvature function.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    #initializing arrays:
    kx_array=np.linspace(kx_left, kx_right, Nx)
    ky_array=np.linspace(ky_left, ky_right, Ny)
    
    # If the curvature function was already computed and saved, load it:
    if load==True:
        data = np.load("data/Chern/curv-func-M-{0}-vs-{1}-u-{2}-kx-{3}-{4}-{5}-ky-{6}-{7}-{8}-omega-{9}.npy".format(M,u,vs,kx_left,kx_right,Nx,ky_left,ky_right,Ny,omega),allow_pickle=True,encoding='latin1')
        Z = np.real(data[()]['curv_func'])

    elif load==False:
        Z = compute_curv_func(M, u, vs, kx_left, kx_right, Nx, ky_left, ky_right, Ny, omega)

    #construct mesh grid
    X, Y = np.meshgrid(kx_array, ky_array)
    
    if vmin=="None":
        vmin=np.amin(Z)

    if vmax=="None":
        vmax=np.amax(Z)

    # Create figure
    fig = plt.figure(figsize=(9,6))
    ax1 = fig.gca()
    fig.suptitle(r'Curvature function $F(k_x,k_y)$'+'\n'+r'for parameters $M=$%s, $u=$%s, $v_s=$%s, and $\omega=$%s'%(M,u,vs,omega) )
    
    # Plot the surface.
    contour = ax1.pcolormesh(X, Y, Z, vmin=vmin, vmax=vmax, cmap=cmaps.viridis)
    ax1.set_xlabel(r'$k_x$')
    ax1.set_ylabel(r'$k_y$')
#    ax1.set_xlim([kx_left,kx_right])
#    ax1.set_ylim([ky_left,ky_right])
    ax1.grid()
    cbar = [fig.colorbar(contour, shrink=0.5, aspect=5)]
    plt.show()
    plt.close()
    
    return
######################################################################################

######################################################################################
def plot_phase_diag_bare(M_start, M_end, M_points, Nx, Ny, load):
    """
        Plot topological phase diagram for the noninteracting case.
        
        Parameters
        ----------
        M_start: float, smallest mass term in the phase diagram

        M_end: float, largest mass term in the phase diagram

        M_points: integer, number of points in the M-direction
               
        Nx: integer, number of points in the momentum sum in x direction.

        Ny: integer, number of points in the momentum sum in y direction.
               
        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots the phase diagram.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    #initializing arrays:
    M_array = np.linspace(M_start,M_end,M_points)

    # If the phase diagram was already computed and saved, load it:
    if load==True:
        data =     np.load("data/Chern/phase-diag-bare-M-{0}-{1}-{2}-Nx-{3}-Ny-{4}-rand.npy".format(M_start,M_end,M_points,Nx,Ny),allow_pickle=True,encoding='latin1')
        Z = data[()]['top_inv_array']
        
    # Otherwise compute if from scratch:
    elif load==False:
        Z = compute_phase_diag_bare(M_start, M_end, M_points, Nx, Ny, True)



    #create figure
    fig = plt.figure(figsize=(9,6))
    ax1 = fig.add_subplot(1,1,1)
    fig.suptitle(r'Noninteracting topological phase diagram with $N_x=$%s, $N_y=$%s'%(Nx,Ny))
    
    # Plot the surface.
    plot = ax1.plot(M_array, Z, 'o-',lw=3)
    ax1.set_xlabel(r'$M$')
    ax1.set_ylabel(r'Chern number $\mathcal{C}$')
    ax1.grid()
    plt.show()
    plt.close()

    return
######################################################################################

######################################################################################
def plot_test_ANN(M_start_training, M_end_training, M_points_training, random_flag, M_initial, M_final, M_points, vs, u_initial, u_final, u_points, Nx, Ny, load, PlotRawData):
    """
        Plot the predicted values (topological phase diagram) from the test set.
        
        Parameters
        ----------
        M_start_training: float, initial value of M in the interval for the training set.
        
        M_end_training: float, final value of M in the interval for the training set.
        
        M_points_training: int, number of points in the M interval for the training set.
        
        random_flag: boolean, flag to choose randomly distributed points in the training set.
                
        M_initial: float, smallest mass term in the data set.

        M_final: float, largest mass term in the data set.

        M_points: integer, number of points in the M-direction.
        
        vs: float, sound velocity.

        u_initial: float, smallest interaction in the data set.

        u_final: float, largest interaction in the data set.

        u_points: integer, number of points in the V-direction.
        
        Nx: integer, number of points in the momentum sum in x direction.

        Ny: integer, number of points in the momentum sum in y direction.
                              
        load: boolean, flag to load previously computed data.
        
        labeled: boolean, flag for calculating the test set labels and obtaining performance data.

        PlotRawData: boolean, flag to plot raw data of the curvature function in the test set.

        Returns
        -------
        none, but plots the predictions/phase diagram.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    # Generate or load the data:
    if load==True:
        data = np.load("data/Chern/curv-func-HSPs-M-{0}-{1}-{2}-vs-{3}-u-{4}-{5}-{6}.npy".format(M_initial,M_final,M_points,vs,u_initial,u_final,u_points), allow_pickle=True, encoding='latin1')
        test_configs  = data[()]['curv_func']
        test_labels  = np.zeros(shape=(u_points,M_points,3))   #bogus (placeholder needed)

    elif load==False:
        test_configs = get_test_set_all_HSPs(M_initial, M_final, M_points, vs, u_initial, u_final, u_points)
        test_labels  = np.zeros(shape=(u_points,M_points,3))   #bogus (placeholder needed)

    # Define the features and labels for the test set from the previously calculated/loaded data:
    X_test = test_configs
    y_test = test_labels
    
    # Obtain predictions from trained ANN:
    predictions_reshaped = test_ANN(X_test, y_test, M_points, M_start_training, M_end_training, M_points_training, u_points, random_flag, False)

    
    # Construct mesh grid
    M_array = np.linspace(M_initial, M_final, M_points)
    u_array = np.linspace(u_initial, u_final, u_points)
    X, Y = np.meshgrid(M_array, u_array)
    
    # Plot first the raw data (the curvature function at the HSPs across parameter space):
    if PlotRawData==True:
        # Canvas:
        fig = plt.figure(figsize=(9,6))
        
        # First panel (HSP=(0,0))
        ax1 = fig.add_subplot(221)
        contour1 = ax1.pcolormesh(X, Y, test_configs[:,:,0], vmin=-10**1, vmax=10**1, cmap=cmaps.viridis)
        cbar1 = plt.colorbar(contour1)  #defines the colorbar
        ax1.set_title(r'Curvature function at the HSP $(0,0)$')
        ax1.set_xlabel(r'$M$')
        ax1.set_ylabel(r'$u$')
        ax1.grid()
        
        # Second panel (HSP=(0,pi))
        ax2 = fig.add_subplot(222)
        contour2 = ax2.pcolormesh(X, Y, test_configs[:,:,1], vmin=-10**2, vmax=10**2, cmap=cmaps.viridis)
        ax2.set_title(r'Curvature function at the HSP $(0, \pi)$')
        cbar2 = plt.colorbar(contour2) #defines the colorbar
        ax2.set_xlabel(r'$M$')
        ax2.set_ylabel(r'$u$')
        ax2.grid()
    
        # Third panel (HSP=(pi,pi))
        ax3 = fig.add_subplot(223)
        contour3 = ax3.pcolormesh(X, Y, test_configs[:,:,2], vmin=10**1, vmax=10**4, cmap=cmaps.viridis)
        ax3.set_title(r'Curvature function at the HSP $(\pi, \pi)$')
        cbar3 = plt.colorbar(contour3) #defines the colorbar
        ax3.set_xlabel(r'$M$')
        ax3.set_ylabel(r'$u$')
        ax3.grid()
    
        plt.show()
        plt.close()

    
    # plot the model predictions (three plots corresponding to the three possible values of the top inv):
    # Create figure
    fig = plt.figure(figsize=(9,6))
    fig.suptitle(r'Predicted topological phase diagram')

    # First panel (probability of predicting phase 1):
    ax1 = fig.add_subplot(221)
    contour1 = ax1.pcolormesh(X, Y, predictions_reshaped[:,:,0], cmap=cmaps.viridis)
    cbar1 = plt.colorbar(contour1)
    ax1.set_xlabel(r'$M$')
    ax1.set_ylabel(r'$u$')
    ax1.set_title(r'Predicted probability for phase 1')
    ax1.grid()
    
    # Second panel (probability of predicting phase 2):
    ax2 = fig.add_subplot(222)
    contour2 = ax2.pcolormesh(X, Y, predictions_reshaped[:,:,1], cmap=cmaps.viridis)
    cbar2 = plt.colorbar(contour2)
    ax2.set_xlabel(r'$M$')
    ax2.set_ylabel(r'$u$')
    ax2.set_title(r'Predicted probability for phase 2')
    ax2.grid()
    
    # Third panel (probability of predicting phase 3):
    ax3 = fig.add_subplot(223)
    contour3 = ax3.pcolormesh(X, Y, predictions_reshaped[:,:,2], cmap=cmaps.viridis)
    cbar3 = plt.colorbar(contour3)
    ax3.set_xlabel(r'$M$')
    ax3.set_ylabel(r'$u$')
    ax3.set_title(r'Predicted probability for phase 3')
    ax3.grid()

    # Put all the predictions together to generate a phase diagram in one plot:
    Z = np.zeros(shape=(u_points,M_points)) #initialize the heatmap array
    for M_idx, M in enumerate(M_array):
        for u_idx, u in enumerate(u_array):
        
            # Determine which phase is most likely from the predictions:
            pred_phase1 = predictions_reshaped[u_idx,M_idx,0]
            pred_phase2 = predictions_reshaped[u_idx,M_idx,1]
            pred_phase3 = predictions_reshaped[u_idx,M_idx,2]
            
            # Save the result as integers -1, 0, or 1 for the three different phases:
            if np.argmax([pred_phase1,pred_phase2,pred_phase3])==0:
                Z[u_idx][M_idx]=-1
            elif np.argmax([pred_phase1,pred_phase2,pred_phase3])==1:
                Z[u_idx][M_idx]=0
            elif np.argmax([pred_phase1,pred_phase2,pred_phase3])==2:
                Z[u_idx][M_idx]=1
        
    # Fourth panel (predicted phase diagram):
    ax4 = fig.add_subplot(224)
    contour4 = ax4.pcolormesh(X, Y, Z, vmax=-1.0, vmin=5.0, cmap='Set2')
    cbar4 = plt.colorbar(contour4)
    ax4.set_xlabel(r'$M$')
    ax4.set_ylabel(r'$u$')
    ax4.set_title(r'Predicted topological phase diagram')
    ax4.grid()

    plt.show()
    plt.close()
    
    return
######################################################################################
