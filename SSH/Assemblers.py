#!/usr/bin/env python
##########################################################################################
# This python module is used to generate the data from 1D and 2D topological insulators that
# is fed to the neural network.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
from __future__ import division
from __future__ import print_function
import numpy as np
###########################################################################################


#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.2"
###########################################################################################


####################################
######     GLOBAL OBJECTS     ######
####################################
###########################################################################################
dk = 10**(-8)*np.pi
###########################################################################################


################################
######     ASSEMBLERS     ######
################################
######################################################################################
def Qk(t, deltat, k):
    """
        Assembles the noninteracting part of the Hamiltonian in k-space for the SSH model.
    
        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat: float, staggered hopping term in the Hamiltonian.

        k: float, momentum.

        Returns
        -------
        Qk: float, noninteracting part of the SSH Hamiltonian in k-space.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    Qk = (t + deltat) + (t - deltat)*np.exp(-1j*k)
    return Qk
######################################################################################
######################################################################################
def phik(t, deltat, V, k):
    """
        Assembles the angle function of the topological invariant for the SSH model.
    
        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat: float, staggered hopping term in the Hamiltonian.

        V: float, interaction strength.

        k: float, momentum.
        
        Returns
        -------
        phik: float, angle function at k for given parameters.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        vertex: V*(1+cos(q))
        
        Note that the self-energy Sigma_AB is the only one entering the calculation
        of the topological invariant.

        Examples
        --------

    """
        
    # Construct the self energy Sigma(q)_AB from the formula given in the reference.
    # The integration is discretized to a sum.
    self_energy_integrand = np.zeros(501,np.complex_)
    for q_idx,q in enumerate(np.linspace(0.0,2*np.pi,501)):
        self_energy_integrand[q_idx] = (V/2)*(1+np.cos(q))*np.exp(1j*np.angle(Qk(t, deltat, k+q)))
    SigmaAB = np.sum(self_energy_integrand)/501
    
    # Construct the angle function from the self-energy and the k-dependent Hamiltonian part Qk.
    phik = -np.angle(Qk(t,deltat,k) + SigmaAB)
        
    return phik
######################################################################################
######################################################################################
def curv_func(t, deltat, V, k):
    """
        Assembles the curvature function.
    
        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat: float, staggered hopping term in the Hamiltonian.

        V: float, interaction strength.

        k: float, momentum.
        
        Returns
        -------
        curv_func: float, curvature function at k for given parameters
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    
    jump = 2*np.pi - 0.5    # threshold for the jump
    # Trick to remove divergences from discontinuities in the angle function:
    # Check if there is a jump:
    if np.abs(phik(t, deltat, V, k+dk) - phik(t, deltat, V, k)) > jump:
    
        # If there is, center the angle function around the earlier value:
        phi_k = phik(t, deltat, V, k)
        phi_kdk = phik(t, deltat, V, k+dk) + np.sign(phik(t, deltat, V, k) - phik(t, deltat, V, k+dk))*2*np.pi
    else:
        phi_k = phik(t, deltat, V, k)
        phi_kdk = phik(t, deltat, V, k+dk)

    curv_func = 1/(2*np.pi)*(phi_k - phi_kdk)/dk
       
    return curv_func
######################################################################################

