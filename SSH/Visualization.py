#!/usr/bin/env python
##########################################################################################
# This python module visualizes the quantities calculated in the other modules.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import numpy as np

#plotting
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors
import colormaps as cmaps

# custom modules:
from .Assemblers import Qk, phik, curv_func
from .Computers import compute_curv_func, compute_phik, compute_top_inv, compute_phase_diag, get_training_set, get_test_set
from .MachineLearning import train_ANN, test_ANN
###########################################################################################


#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.1"
###########################################################################################



################################
######      PLOTTERS      ######
################################
######################################################################################
def plot_phik(t, deltat, V, k_left, k_right, N, load=False):
    """
        Plot curvature function in k-space at a given set of parameters.
        
        Parameters
        ----------
        t: float, hopping term.
        
        deltat: float, staggered hopping term.

        V: float, interaction strength.
         
        k_left: float, lower bound for the k values.

        k_right: float, upper bound for the k values.
        
        N: integer, number of points in k direction.

        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots the curvature function
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    #initializing arrays:
    k_array=np.linspace(k_left, k_right, N)
    
    if load==False:
        Z = compute_phik(t, deltat, V, k_left, k_right, N)

    elif load==True:
        data = np.load("data/SSH/SSH-phik-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}-N-{6}.npy".format(deltat_start,deltat_end,deltat_points,V_start,V_end,V_points,N),allow_pickle=True,encoding='latin1')
        Z = np.real(data[()]['curv_func'])

    jump = 2*np.pi-0.5
    for k_idx, k in enumerate(k_array[:-1]):
        if np.abs(Z[k_idx] - Z[k_idx+1]) > jump:
            #Z[k_idx+1] = (phik(t, deltat, V, k + np.pi + np.abs(k_right-k_left)/N) + phik(t, deltat, V, k) ) % (2*np.pi) - np.pi - phik(t, deltat, V, k)
            Z[k_idx+1] = Z[k_idx+1] + np.sign(Z[k_idx]-Z[k_idx+1])*2*np.pi


    #create figure
    fig = plt.figure(figsize=(9,6))
    ax1 = fig.add_subplot(111)
    fig.suptitle(r'Angle functions $\phi(k)$'+'\n'+r'for parameters $\delta_t=$%s and $V=$%s'%(deltat,V) )
    
    # Plot the surface.
    contour = ax1.plot(k_array, Z, 'b-', lw=3, label=r'$\phi(k)$')
    ax1.set_xlabel(r'$k$')
    ax1.set_ylabel(r'$\phi(k)$')
    ax1.set_xlim([k_left,k_right])
    ax1.grid()
    plt.legend(loc='best')
    plt.show()
    plt.close()
    
    return
######################################################################################
######################################################################################
def plot_curv_func(t, deltat, V, k_left, k_right, N, vmin=-1, vmax=1, load=False):
    """
        Plot curvature function in k-space at a given set of parameters.
        
        Parameters
        ----------
        t: float, hopping term.
        
        deltat: float, staggered hopping term.

        V: float, interaction strength.
         
        k_left: float, lower bound for the k values.

        k_right: float, upper bound for the k values.
        
        N: integer, number of points in k direction.
                   
        vmin: integer, minimal value to show in the plot.

        vmax: integer, maximal value to show in the plot.

        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots the curvature function
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    #initializing arrays:
    k_array=np.linspace(k_left, k_right, N)
    
    if load==False:
        Z = compute_curv_func(t, deltat, V, k_left, k_right, N)

    elif load==True:
        data = np.load("data/SSH/SSH-curv-func-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}-N-{6}.npy".format(deltat_start,deltat_end,deltat_points,V_start,V_end,V_points,N),allow_pickle=True,encoding='latin1')
        Z = np.real(data[()]['curv_func'])

    #create figure
    fig = plt.figure(figsize=(9,6))
    ax1 = fig.add_subplot(111)
    fig.suptitle(r'Curvature function $F(k)$'+'\n'+r'for parameters $\delta_t=$%s and $V=$%s'%(deltat,V) )
    
    # Calculate the value of the topological invariant in the given k-interval
    inv = 1/(2*np.pi)*np.sum(Z)*(k_right-k_left)/N

    # Plot the surface.
    contour = ax1.plot(k_array, Z, 'b-', lw=4, label=r"Integral: %s"%(np.round(inv,2)))
    ax1.set_xlabel(r'$k$')
    ax1.set_ylabel(r'$F(k)$')
    ax1.set_xlim([k_left,k_right])
    if vmax is None:
        vmax = np.amax(Z)
    if vmin is None:
        vmin = np.amin(Z)
        # enlarge slightly the minima to fit the curve nicely into the plot:
        if vmin > 0:
            vmin = 0.99*vmin
        elif vmin < 0:
            vmin = 1.01*vmin
    ax1.set_ylim([vmin,vmax])
    ax1.grid()
    plt.legend()
    plt.show()
    plt.close()
    
    return
######################################################################################
#######################################
######################################################################################
def plot_phase_diag(t, deltat_start, deltat_end, deltat_points, V_start, V_end, V_points, N, vmin=0.0, vmax=1.0, load=False):
    """
        Plot topological phase diagram.
        
        Parameters
        ----------
        deltat_start: float, smallest staggered hopping term in the phase diagram

        deltat_end: float, largest staggered hopping  term in the phase diagram

        deltat_points: integer, number of points in the deltat-direction

        V_start: float, smallest interaction in the phase diagram

        V_end: float, largest interaction in the phase diagram

        V_points: integer, number of points in the M-direction
               
        N: integer, number of points in the momentum sum.
        
        vmin: minimal value of the topological invariant in the phase diagram.
        
        vmax: maximal value of the topological invariant in the phase diagram.
               
        load: boolean, flag to load previously computed data.

        Returns
        -------
        none, but plots the phase diagram.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    #initializing arrays:
    deltat_array = np.linspace(deltat_start,deltat_end,deltat_points)
    V_array = np.linspace(V_start,V_end,V_points)

    if load==False:
        Z = compute_phase_diag(t, deltat_start, deltat_end, deltat_points, V_start, V_end, V_points, N, True)

    elif load==True:
        data =     np.load("data/SSH/SSH-phase-diag-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}-N-{6}.npy".format(deltat_start,deltat_end,deltat_points,V_start,V_end,V_points,N),allow_pickle=True,encoding='latin1')
        Z = data[()]['top_inv_array']


    #construct mesh grid
    X, Y = np.meshgrid(deltat_array, V_array)
    

    #create figure
    fig = plt.figure(figsize=(9,6))
    ax1 = fig.gca()
    fig.suptitle(r'Topological phase diagram')
    
    # Plot the surface.
    contour = ax1.pcolormesh(X, Y, Z, vmin=vmin, vmax=vmax, cmap=cmaps.viridis)
    ax1.set_xlabel(r'$\delta_t$')
    ax1.set_ylabel(r'$V$')
    ax1.grid()
    cbar = [fig.colorbar(contour, shrink=0.5, aspect=5)]
    plt.show()
    
    return
######################################################################################

######################################################################################
def plot_test_ANN(t, deltat_initial_training, deltat_final_training, deltat_points_training, deltat_initial, deltat_final, deltat_points, V_initial, V_final, V_points, load, plot_curv_func = False):
    """
        Plot the predicted values (topological phase diagram) from the test set.
        
        Parameters
        ----------
        t: float, the hopping strength.
        
        deltat_initial_training: integer, initial value of deltat in the interval used for training.

        deltat_final_training: integer, final value of deltat in the interval used for training.

        deltat_points_training: integer, number of points used in the training.

        deltat_initial: float, smallest staggered hopping term in the data set.

        deltat_final: float, largest staggered hopping term in the data set.

        deltat_points: integer, number of points in the deltat-direction.
        
        V_initial: float, smallest interaction in the data set.

        V_final: float, largest interaction in the data set.

        V_points: integer, number of points in the V-direction.
                                      
        load: boolean, flag to load previously computed data.
        
        plot_curv_func: boolean, flag to plot the behavior of the curvature function across parameter space.

        Returns
        -------
        none, but plots the predictions/phase diagram.
        
        References
        ----------

        Notes
        -----

        Examples
        --------

     """

    # Generate or load the data:
    if load==True:
        data =     np.load("data/SSH/SSH-curv-func-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}.npy".format(deltat_initial,deltat_final,deltat_points,V_initial,V_final,V_points), allow_pickle=True, encoding='latin1')
        X_test  = data[()]['curv_func']
    elif load==False:
        X_test = get_test_set(t, deltat_initial, deltat_final, deltat_points, V_initial, V_final, V_points)

    # Construct mesh grid
    deltat_array = np.linspace(deltat_initial, deltat_final, deltat_points)
    V_array = np.linspace(V_initial, V_final, V_points)
    X, Y = np.meshgrid(deltat_array, V_array)
    
    if plot_curv_func == True:
    
        # Plot first the raw data
        fig = plt.figure(figsize=(9,6))
        fig.suptitle(r'Curvature function at the HSP $k=\pi$')
        ax1 = fig.add_subplot(111)
        # Plot the surface.
        contour1 = ax1.pcolormesh(X, Y, X_test, vmin=-1.0, vmax=1.0, cmap=cmaps.viridis)
        cbar1 = plt.colorbar(contour1)
        ax1.set_xlabel(r'$\delta_t$')
        ax1.set_ylabel(r'$V$')
        ax1.grid()
        plt.show()
        plt.close()

        #make an array that only captures the sign of the curvature function at the HSP:
        sign_array = np.zeros(shape=(V_points,deltat_points))
        for deltat_idx, deltat in enumerate(deltat_array):
            for V_idx, V in enumerate(V_array):
                if X_test[V_idx,deltat_idx] > 0:
                    sign_array[V_idx,deltat_idx] = 1
                elif X_test[V_idx,deltat_idx] < 0:
                    sign_array[V_idx,deltat_idx] = -1


        #plot where the function is positive/negative:
        fig = plt.figure(figsize=(9,6))
        fig.suptitle(r'Sign of the curvature function at the HSP $k=\pi$')

        ax1 = fig.add_subplot(111)
        # Plot the surface.
        contour1 = ax1.pcolormesh(X, Y, sign_array, cmap=cmaps.viridis)
        cbar1 = plt.colorbar(contour1)
        ax1.set_xlabel(r'$\delta_t$')
        ax1.set_ylabel(r'$V$')
        ax1.grid()
        plt.show()
        plt.close()
    

    # Obtain predictions from trained ANN:
    predictions_reshaped = test_ANN(X_test, deltat_initial_training, deltat_final_training,  deltat_points_training, deltat_points, V_points)

    #plot the model predictions (three plots corresponding to the three possible values of the top inv):
    print(np.shape(predictions_reshaped))

    
    #create figure
    fig = plt.figure(figsize=(9,6))
    fig.suptitle(r'Predicted topological phase diagram')

    ax1 = fig.add_subplot(221)
    # Plot the surface.
    contour1 = ax1.pcolormesh(X, Y, predictions_reshaped[:,:,0], cmap=cmaps.viridis)
    cbar1 = plt.colorbar(contour1)
    ax1.set_xlabel(r'$\delta_t$')
    ax1.set_ylabel(r'$V$')
    ax1.set_title(r'Predicted probability for phase 1')
    ax1.grid()
    
    ax2 = fig.add_subplot(222)
    # Plot the surface.
    contour2 = ax2.pcolormesh(X, Y, predictions_reshaped[:,:,1], cmap=cmaps.viridis)
    cbar2 = plt.colorbar(contour2)
    ax2.set_xlabel(r'$\delta_t$')
    ax2.set_ylabel(r'$V$')
    ax2.set_title(r'Predicted probability for phase 2')
    ax2.grid()
    

    Z = np.zeros(shape=(V_points, deltat_points))
    #put all the predictions together to generate a phase diagram in one plot:
    for deltat_idx, deltat in enumerate(deltat_array):
        for V_idx, V in enumerate(V_array):
            #determine which is the most likely phase from the predictions:
            pred_phase1 = predictions_reshaped[V_idx,deltat_idx,0]
            pred_phase2 = predictions_reshaped[V_idx,deltat_idx,1]
            #save the result as integers 1 to 3 for the three different phases:
            if np.argmax([pred_phase1,pred_phase2])==0:
                Z[V_idx][deltat_idx]=0
            elif np.argmax([pred_phase1,pred_phase2])==1:
                Z[V_idx][deltat_idx]=1
                
    ax4 = fig.add_subplot(223)
    # Plot the surface.
    contour4 = ax4.pcolormesh(X, Y, Z, vmin=0.0, vmax=6.0, cmap='Set1')
    cbar4 = plt.colorbar(contour4)
    ax4.set_xlabel(r'$\delta_t$')
    ax4.set_ylabel(r'$V$')
    ax4.set_title(r'Predicted topological phase diagram')
    ax4.grid()

    plt.show()
    plt.close()
    
    return
######################################################################################