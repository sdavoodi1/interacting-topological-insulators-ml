#!/usr/bin/env python
##########################################################################################
# This python program uses the keras library to perform phase classification of
# topological phases of an interacting SSH model.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import numpy as np
import scipy.integrate as integrate

#plotting
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors
import colormaps as cmaps

#machine learning packages (keras and tensorflow)
import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import Dense, Dropout,Conv2D,MaxPooling2D,Flatten, BatchNormalization, LeakyReLU, Conv2DTranspose,AveragePooling2D
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import regularizers

# custom modules:
from .Assemblers import Qk, phik, curv_func
from .Computers import get_training_set, get_test_set
###########################################################################################





#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.1"
###########################################################################################







####################################
######     GLOBAL OBJECTS     ######
####################################
###########################################################################################
###########################################################################################


######################################################################################
def train_ANN(t, deltat_initial, deltat_final, deltat_points, width, epochs, batch_size, random_flag=True, shuffle=True, load=False):
    """
        Computes the topological invariant across a range of parameters to yield a phase diagram.
       
        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat_initial: float, smallest staggered hopping term in the phase diagram

        deltat_final: float, largest staggered hopping term in the phase diagram

        deltat_points: integer, number of points in the deltat-direction

        width: integer, number of neurons in the hidden layer.

        epochs: integer, number of epochs the network will be trained for.

        batch_size: integer, size of the batches fed to the neural network.

        random_flag: boolean, flag for using random points in the M interval for training.

        shuffle: boolean, flag for shuffling the training data.
    
        load: boolean, flag to load previously computed training data.


        Returns
        -------
        model: keras model, the model trained with the provided data from the M-interval.
           
           
        References
        ----------


        Notes
        -----
        Empirically we find that no regularizers give better performance.


        Examples
        --------

    """

    # Load previously calculated input data, or generate it on the fly:
    if load==True:
        if random_flag==True:
            data = np.load("data/SSH/SSH-curv-func-top-inv-deltat-{0}-{1}-{2}-rand.npy".format(deltat_initial,deltat_final,deltat_points), allow_pickle=True, encoding='latin1')
        else:
            data = np.load("data/SSH/SSH-curv-func-top-inv-deltat-{0}-{1}-{2}.npy".format(deltat_initial,deltat_final,deltat_points), allow_pickle=True, encoding='latin1')

        train_features  = data[()]['curv_func']
        train_labels  = data[()]['top_inv']
    
    elif load==False:
        train_features, train_labels = get_training_set(t, deltat_initial, deltat_final, deltat_points, random_flag)


    print("train features:", train_features)
    print("train labels:",train_labels)

    # Prepare data for training:
    # 1) shuffle data with the same sequence.
    # turn label vector into a 1xN matrix to enable simultaneous shuffling:
    train_labels = np.array([train_labels])
    if shuffle==True:
        #concatenate data:
        full_data = np.concatenate((train_features,train_labels.T),axis=1)
        #shuffling:
        np.random.seed()
        np.random.shuffle(full_data)
        #re-split data into feature vector and labels:
        train_features = full_data[:,:-1]
        train_labels = full_data[:,-1:].T
    
    #rewrite labels as 2-dim vector for the two phases
    y = []
    for i, label in enumerate(train_labels[0]):
        if label==0:
            y.append((1, 0))   # phase 1
        elif label==1:
            y.append((0, 1))   # phase 2


    X = train_features
    y = np.array(y)
    
    # Create model and train it (via subroutines):
    model = CreateModel(width)
    model = TrainModel(model, X, y, deltat_initial, deltat_final, deltat_points, epochs, batch_size, random_flag)

    return model
######################################################################################


######################################################################################
def CreateModel(width):
    """
        Create a keras model to perform phase classification for the interacting SSH model.
       
        Parameters
        ----------
        width: int, width of the hidden layer (number of neurons).


        Returns
        -------
        model: keras model, the model architecture (a simple fully connected network with one hidden layer).
           
           
        References
        ----------


        Notes
        -----
            
            
        Examples
        --------

    """
    
    # Create the NN model.
    # Network architecture: input layer (single number), hidden layer with width neurons, and output layer with 2 neurons.
    model = Sequential()
    model.add(Dense(width, input_dim=1, kernel_initializer='glorot_normal', activation='sigmoid', kernel_regularizer=regularizers.l2(0.0)))
    model.add(Dense(2, kernel_initializer='glorot_normal', activation='softmax', kernel_regularizer=regularizers.l2(0.0)))
    
    return model
######################################################################################


######################################################################################
def TrainModel(model, X, y, deltat_initial, deltat_final, deltat_points, epochs, batch_size, random_flag):
    """
        Trains the neural network with the provided training data.
       
        Parameters
        ----------
        X: numpy array, training data (features).

        y: numpy array, training labels.
        
        deltat_initial: float, smallest staggered hopping term in the phase diagram

        deltat_final: float, largest staggered hopping term in the phase diagram

        deltat_points: integer, number of points in the deltat-direction

        epochs: integer, number of epochs the network will be trained for.

        batch_size: integer, size of the batches fed to the neural network.

        random_flag: boolean, flag for using random points in the M interval for training.


        Returns
        -------
        model: keras model, the model trained with the provided data from the M-interval.
        
           
        References
        ----------


        Notes
        -----
            
            
        Examples
        --------

    """
    # Compile model and print a summary:
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()

    # Train model:
    model.fit(X, y, epochs=epochs, batch_size=batch_size)
    
    # Saving model:
    if random_flag==True:
        model.save('data/SSH/trained_model-deltat-{0}-{1}-{2}-rand'.format(deltat_initial,deltat_final,deltat_points))

    elif random_flag==False:
        model.save('data/SSH/trained_model-deltat-{0}-{1}-{2}'.format(deltat_initial,deltat_final,deltat_points))

    return model
######################################################################################


######################################################################################
def test_ANN(X_test, deltat_initial_training, deltat_final_training,  deltat_points_training, deltat_points, V_points):
    """
        Computes the topological invariant across a range of parameters to yield a phase diagram.
       
        Parameters
        ----------
        X_test: numpy array, test data (features).

        deltat_initial_training: integer, initial value of deltat in the interval used for training.

        deltat_final_training: integer, final value of deltat in the interval used for training.

        deltat_points_training: integer, number of points used in the training.

        deltat_points: integer, number of points in the deltat-direction (for testing).

        V_points: integer, number of points in the V-direction.
    
        Returns
        -------
        model: keras model, the model trained with the provided data from the M-interval.
           
        References
        ----------

        Notes
        -----
        
        eval=True gives sensible results only if true labels are provided.
    
        Examples
        --------

    """
    

    #load model:
    model = load_model('data/SSH/trained_model-deltat-{0}-{1}-{2}-rand'.format(deltat_initial_training,deltat_final_training,deltat_points_training))
        # reshape the test data to a structure that can be accepted by the neural network
        # (merge all data points from different deltat and V in the same dimension).
    X_test_reshaped = X_test.reshape(deltat_points*V_points)

    #Evaluate model.
    predictions = model.predict(X_test_reshaped)
    predictions_reshaped = predictions.reshape(V_points,deltat_points,2)
    
    return predictions_reshaped
######################################################################################
